from django.urls import path
from receipts.views import home, create_receipt, categories,accounts, create_category, create_account


urlpatterns = [
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", accounts, name="account_list"),
    path("categories/", categories, name="category_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", home, name="home")
]
